#!/bin/bash
set -xeuo pipefail
IFS=$'\n\t'

POSTGRES_VERSION=${1:-12}

function build_debian() {
    DEBIAN_VERSION=$(lsb_release -c -s)

    export DEBIAN_FRONTEND=noninteractive

    # Uninstall the system client so that we don't have multiple versions
    apt purge -y postgresql-client postgresql-client-common

    curl -sS -L https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add -
    echo "deb http://apt.postgresql.org/pub/repos/apt/ ${DEBIAN_VERSION}-pgdg main" | tee /etc/apt/sources.list.d/postgresql.list

    apt-get update
    apt-get install -y postgresql-client-${POSTGRES_VERSION}

    apt-get autoremove -yq
    apt-get clean -yqq
    rm -rf /var/lib/apt/lists/*
}

BUILD_OS=${BUILD_OS:-debian}

if [[ $BUILD_OS =~ debian ]]; then
    build_debian "$@"
elif [[ $BUILD_OS =~ ubi ]]; then
    build_ubi "$@"
fi
